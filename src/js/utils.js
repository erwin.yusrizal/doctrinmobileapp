/* jshint esversion: 6 */

import moment from 'moment';

export function validEmail(email) {
    const re = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    return re.test(email);
}

export function validPhone(phone){
    return /\(?(?:\+62|62|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}/g.test(phone);
}

export function convertAmpersand(text){
    return text.replace(/&amp;/g, '&');
}

export function dateFormat(date, format){
    moment.locale('id_id');
    format = format || 'DD/MM/YYYY HH:mm';
    return moment(date).format(format);
}

export function hoursAgo(date){
    moment.locale('id_id');
    let now = moment(new Date());
    let end = moment(date);
    let duration = moment.duration(now.diff(end));
    let days = duration.asDays();

    if(days >= 1){
        return moment(date).format('DD MMM YYYY HH:mm');
    }else{
        return moment(date).format('HH:mm');
    }
}

export function capitalize(str){
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

export function truncate(str, len){
    len = len || 100;
    return str.length > len ? str.substring(0, len) + '...': str;
}

export function stripHTML(str){
    let regex = /(<([^>]+)>)/ig;
    return str.replace(regex, "");
}

export function initial(str){
    return Array.prototype.map.call(str.split(" "), function(x,y){ return y < 2 ? x.substring(0,1).toUpperCase():'';}).join('');
}

export function parseError(e){
    let error = {};

    for(const key in e.messages){
        const field  = key.replace('_', ' ');
        error.field = capitalize(field);
        error.messages = e.messages[key].join(', ');
    }

    return error;
}

export function slugify(text){
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')
        .replace(/[^\w\-]+/g, '')
        .replace(/\-\-+/g, '-')
        .replace(/^-+/, '')
        .replace(/-+$/, '');
}

export function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

export function bytesToSize(bytes) {
	var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	if (bytes == 0) return '0 Byte';
	var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}