/* jshint esversion: 6 */

import Vue from 'vue';

export const getDocuments = (payload) => {
    return Vue.http.get('/documents', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewDocument = (payload) => {
    return Vue.http.get('/documents/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editDocument = (payload) => {
    return Vue.http.put('/documents/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const updateDocumentStatus = (payload) => {
    return Vue.http.put('/documents/'+payload.id+'/status', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const trackDocument = (payload) => {
    return Vue.http.get('/tracking', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};