/* jshint esversion: 6 */

import Vue from 'vue';
import store from '../store';

export const login = (payload) => {
    return Vue.http.post('/auth/login', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const verify = (payload) => {
    return Vue.http.post('/auth/verify', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const reverify = (payload) => {
    return Vue.http.put('/auth/verify', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const refresh = (payload) => {
    let { refresh_token } = store.getters['auth/token'];
    return Vue.http.put('/auth/login', {
        headers: {
            'Authorization': 'Bearer ' + refresh_token
        }
    })
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

function accessRevoke(){
    let { access_token } = store.getters['auth/token'];
    return Vue.http.delete('/auth/revoke/access', {
        headers: {
            'Authorization': 'Bearer ' + access_token
        }
    });
}

function refreshRevoke(){
    let { refresh_token } = store.getters['auth/token'];
    return Vue.http.delete('/auth/revoke/refresh', {
        headers: {
            'Authorization': 'Bearer ' + refresh_token
        }
    });
}

export const logout = (payload) => {    
    return Vue.http.all([accessRevoke(), refreshRevoke()])
        .then(Vue.http.spread((access, refresh) => {
            return {
                logout: {
                    access: access,
                    refresh: refresh
                }
            };
        }));
};

export const device = (payload) => {
    return Vue.http.put('/auth/device', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const updateMe = (payload) => {
    return Vue.http.put('/users/me', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getNotifications = (payload) => {
    return Vue.http.get('/users/notifications', {params:payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};