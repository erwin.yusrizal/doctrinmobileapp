/* jshint esversion:6 */

import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

import {device} from '../services/auth';
import auth from './modules/auth';
import history from './modules/history';
import document from './modules/document';

Vue.use(Vuex);

export default new Vuex.Store({
    plugins: [createPersistedState({
        paths: ['nid', 'auth.user', 'auth.token']
    })],
    state: {
        nid: ''
    },
    getters: {
        nid: state => state.id
    },
    mutations: {
        ADD_DEVICE: (state, nid) => {
            state.nid = nid;
        },
    },
    actions: {
        async addDevice({ commit }, payload){
            try{
                let response = await device(payload);
                let data = response.data;
                commit('ADD_DEVICE', data.nid);
                return data;
            }catch(e){}
        },
    },
    modules: {
        auth,
        history,
        document
    }
});