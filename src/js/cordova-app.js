var cordovaApp = {
  f7: null,
  /*
  This method hides splashscreen after 2 seconds
  */
  handleSplashscreen: function() {
    var f7 = cordovaApp.f7;
    if (!window.navigator.splashscreen || f7.device.electron) return;
    setTimeout(() => {
      window.navigator.splashscreen.hide();
    }, 2000);
  },
  /*
  This method prevents back button tap to exit from app on android.
  And allows to exit app on backbutton double tap
  */
  handleAndroidBackButton: function () {
    var f7 = cordovaApp.f7;
		var $ = f7.$;
		if (f7.device.electron) return;
		document.addEventListener('backbutton', function (e) {
			var mainView = f7.views.main;
			var leftp = f7.panel.left && f7.panel.left.opened;
			var rightp = f7.panel.right && f7.panel.right.opened;
			if ( leftp || rightp ) {
				f7.panel.close();
				return false;
			} else if ($('.modal-in').length > 0) {
				f7.dialog.close();
				return false;
			} else if (f7.views.main.router.url == '/') {
				const tab = $('.toolbar-bottom .tab-link-active').data('tab');
				if( tab == '#view-home'){
					f7.dialog.confirm('Are you sure want to quit?', 'Quit', function() {
						navigator.app.exitApp();
					});
				}else{
					$('.toolbar-bottom .tab-link[data-tab="'+tab+'"]').trigger('click');
				}
				
			} else {
				mainView.router.back();
			}
		}, false);
  },
  /*
  This method does the following:
    - provides cross-platform view "shrinking" on keyboard open/close
    - hides keyboard accessory bar for all inputs except where it required
  */
  handleKeyboard: function () {
    var f7 = cordovaApp.f7;
    if (!window.Keyboard || !window.Keyboard.shrinkView || f7.device.electron) return;
    var $ = f7.$;
    window.Keyboard.shrinkView(false);
    window.Keyboard.disableScrollingInShrinkView(true);
    window.Keyboard.hideFormAccessoryBar(true);
    window.addEventListener('keyboardWillShow', () => {
      f7.input.scrollIntoView(document.activeElement, 0, true, true);
    });
    window.addEventListener('keyboardDidShow', () => {
      f7.input.scrollIntoView(document.activeElement, 0, true, true);
    });
    window.addEventListener('keyboardDidHide', () => {
      if (document.activeElement && $(document.activeElement).parents('.messagebar').length) {
        return;
      }
      window.Keyboard.hideFormAccessoryBar(false);
    });
    window.addEventListener('keyboardHeightWillChange', (event) => {
      var keyboardHeight = event.keyboardHeight;
      if (keyboardHeight > 0) {
        // Keyboard is going to be opened
        document.body.style.height = `calc(100% - ${keyboardHeight}px)`;
        $('html').addClass('device-with-keyboard');
      } else {
        // Keyboard is going to be closed
        document.body.style.height = '';
        $('html').removeClass('device-with-keyboard');
      }

    });
    $(document).on('touchstart', 'input, textarea, select', function (e) {
      var nodeName = e.target.nodeName.toLowerCase();
      var type = e.target.type;
      var showForTypes = ['datetime-local', 'time', 'date', 'datetime'];
      if (nodeName === 'select' || showForTypes.indexOf(type) >= 0) {
        window.Keyboard.hideFormAccessoryBar(false);
      } else {
        window.Keyboard.hideFormAccessoryBar(true);
      }
    }, true);
  },
  handleDeviceId: function(){
		var notificationOpenedCallback = function(jsonData) {
            // console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
        };

        window.plugins.OneSignal
        .startInit("6bc67df0-7c2c-4b78-8719-c17eb0f94766")
        .handleNotificationOpened(notificationOpenedCallback)
        .endInit();

        window.plugins.OneSignal.getPermissionSubscriptionState(function(s){
            window.localStorage.setItem('nid', s.subscriptionStatus.userId);
        });
	},
  init: function (f7) {
    // Save f7 instance
    cordovaApp.f7 = f7;

    // Handle Android back button
    cordovaApp.handleAndroidBackButton();

    // Handle Statusbar
    cordovaApp.handleSplashscreen();

    // Handle Keyboard
    cordovaApp.handleKeyboard();

    // Handle Device
		cordovaApp.handleDeviceId();
  },
};
export default cordovaApp;
