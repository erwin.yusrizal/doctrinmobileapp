// Import Vue
import Vue from 'vue';
import axios from 'axios';
import store from './store';
import Framework7 from 'framework7/framework7.esm.bundle.js';
import Framework7Vue from 'framework7-vue/framework7-vue.esm.bundle.js';
import VueCountdown from '@chenfengyuan/vue-countdown';
import VueQrcode from '@chenfengyuan/vue-qrcode';
import 'framework7/css/framework7.bundle.css';
import '../css/icons.css';
import '../css/app.scss';

// Import App Component
import App from '../components/app.vue';

// Init Framework7-Vue Plugin
Framework7.use(Framework7Vue);
Vue.component(VueCountdown.name, VueCountdown);
Vue.component(VueQrcode.name, VueQrcode);

// Default
Number.prototype.format = function(n, x, s, c) {
  var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
	  num = this.toFixed(Math.max(0, ~~n));

  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

String.prototype.capitalize = function(){
return this.replace(/\w\S*/g, function(txt){
	  return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
};

Number.prototype.kformat = function(){
	return Math.abs(this) > 999 ? Math.sign(this)*((Math.abs(this)/1000).toFixed(1)) + 'k' : Math.sign(this)*Math.abs(this);
};

/***
* Axios Setup
*/

Vue.http = axios.create({
	baseURL: 'https://api.doctrin.pusri.co.id/v1',
	withCredentials: true,
	headers: {
		'X-Requested-With': 'XMLHttpRequest',
		'Content-Type': 'application/json'
	}
});

// Intercept Request
Vue.http.interceptors.request.use(function (config) {
	if(localStorage.getItem('nid') != null){
		config.headers.common.nid = localStorage.getItem('nid');
	}
	if(store.getters['auth/isLoggedIn']){
		if(config.url.indexOf('auth/revoke/refresh') == -1 && config.url.indexOf('auth/refresh') == -1){
			config.headers.Authorization = 'Bearer ' + store.state.auth.token.access_token;
		}else if(config.url.indexOf('auth/revoke/refresh') > -1){
			localStorage.removeItem('vuex');
		}
	}
	return config;

}, function (error) {
	if(error.response.status == 401){
		store.dispatch('auth/logout');
	}
	return Promise.reject(error.response.data);
});

//Intercept Response
Vue.http.interceptors.response.use(function (response) {
	if(response.config.url.indexOf('auth/revoke/refresh') > -1){
		location.reload();
	}
	return response.data;

}, function (error) {
	if(error.response.status == 401){
		store.dispatch('auth/logout');
	}
	return Promise.reject(error.response.data);
});

// Init App
new Vue({
  	el: '#app',
  	store: store,
  	render: (h) => h(App),
  	// Register App Component
  	components: {
		app: App
  	},
});