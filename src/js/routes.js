/* jshint esversion:6 */

import Vue from 'vue';
import store from './store';
import LoginPage from '../pages/login.vue';
import NotFoundPage from '../pages/404.vue';

var routes = [
	{
		path: '/',
		async: function (routeTo, routeFrom, resolve, reject) {
			const router = this;
			const HomePage = () => import('../pages/home.vue');

			HomePage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/login/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const LoginPage = () => import('../pages/login.vue');

			LoginPage().then((vc) => {
				resolve({ component: vc.default });
			});			
		}
	},
	{
		path: '/document/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {
			const router = this;
			const DocumentPage = () => import('../pages/document.vue');

			DocumentPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/document/:id/status/',
		async: function (routeTo, routeFrom, resolve, reject) {
			const router = this;
			const DocumentStatusPage = () => import('../pages/documents-status.vue');

			DocumentStatusPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/scan/',
		async: function (routeTo, routeFrom, resolve, reject) {
			const router = this;
			const ScanPage = () => import('../pages/scan.vue');

			ScanPage().then((vc) => {
				resolve({ component: vc.default });	
			});			
		}
	},	
	{
		path: '/account/',
		async: function (routeTo, routeFrom, resolve, reject) {
			const router = this;
			const AccountPage = () => import('../pages/account.vue');

			AccountPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/account/edit/',
		async: function (routeTo, routeFrom, resolve, reject) {
			const router = this;
			const AccountEditPage = () => import('../pages/account-form.vue');

			AccountEditPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/account/histories/',
		async: function (routeTo, routeFrom, resolve, reject) {
			const router = this;
			const AccountHistoriesPage = () => import('../pages/account-histories.vue');

			AccountHistoriesPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '(.*)',
		component: NotFoundPage,
	},
];

export default routes;
